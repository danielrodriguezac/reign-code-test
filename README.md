# Reign Code Test

## Running the image
This code test was developed and tested on a POSIX compliant system with bash as the default shell


### Pre-requisites
Must have both docker and docker-compose installed see:

* [Installing docker](https://lmgtfy.com/?q=install+docker)
* [Installing docker-compose](https://lmgtfy.com/?q=install+docker+compose)

### Spawning the container

Using sudo will depend on your system configuration:

```
sudo docker-compose -f "docker-compose.yml" up --build
```

This will spawn the container attached to stdin/out, add `-d` to run as daemon 

```
sudo docker-compose -f "docker-compose.yml" up -d --build
```
Then check the app by pointing your browser to

    http://localhost:3000

## Code testing/coverage

Tests and coverage are handled on the backend by nodejs:assert, mocha and nyc. Most tests require a valid mongoDB instance running on the default path see: 
[link](#markdown-header-run-mongodb-without-composing-for-testing-purposes)

### Test + Coverage

This assumes there is a mongodb server running with the default connection parameters

    cd reign-backend
    npm run coverage

You can however pass the database parameters (careful, don't wanna break that db of yours ;)

    MONGODB_DATABASE_NAME=testdb \
    MONGODB_STORIES_COLLECTION_NAME=testCollection \
    MONGODB_URL=mongodb://localhost:27017 \
    npm run coverage

## Run mongoDB without composing for testing purposes

Mind the directory you want to target, for it will be written at as a superuser (again, depending on your docker setup)

    sudo docker run -d -p 27017:27017 \
    -v ~/var/db/reign-code-test/mongodb-data/:/data/db mongo