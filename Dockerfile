FROM node:14 as angularBuilder

WORKDIR /var/build/angularApp
COPY ./reign-frontend/package*.json ./
RUN npm install
COPY ./reign-frontend/. .
RUN npm run build4prod

FROM node:14

WORKDIR /usr/bin/server
COPY ./reign-backend/package*.json ./
RUN npm install
COPY ./reign-backend/. .
COPY --from=angularBuilder /var/build/angularApp/dist/reign-frontend ./public/.

EXPOSE 3000
CMD [ "npm", "start" ]