const https = require('https');
const crypto = require('crypto');

const MongoClient = require('mongodb').MongoClient;
const { MONGODB_URL, MONGODB_DATABASE_NAME, MONGODB_STORIES_COLLECTION_NAME, MONGODB_CLIENT_CONFIG } = require('./mongodb-configurator.js')();

function acquire() {
    return new Promise((resolve, reject) => {
        // console.log(`Starting API request`);
        let dataBuffer = '';
        // Make component testable with dummy data
        let useablehttpsReference = this.https || https;
        const req = useablehttpsReference
            .get('https://hn.algolia.com/api/v1/search_by_date?query=nodejs', (res) => {
                // console.log(`Connection with API stablished...`);
                res.on('data', (data) => {
                    // console.log(`Received a data chunk of size: ${data.length}`);
                    dataBuffer += data;
                });
                res.on('end', () => {
                    processData(dataBuffer, { resolve, reject });
                });
            })
            .on('error', (err) => {
                // console.error(`Error during request: ${err}`);
                reject(err);
            });
    });

}
async function processData(dataBuffer, resolutions) {
    // console.log(dataBuffer);
    // console.log(`End signal received, parsing...`);
    let parsedResponse;
    try {
        parsedResponse = JSON.parse(dataBuffer);
    } catch (err) {
        // console.error(`Error parsing server response`, err);
        resolutions.reject(err);
        return;
    }
    // Connect to mongoDB
    const client = new MongoClient(MONGODB_URL, MONGODB_CLIENT_CONFIG);
    let database;
    let collection;
    try {
        await client.connect();
        database = client.db(MONGODB_DATABASE_NAME);
        collection = database.collection(MONGODB_STORIES_COLLECTION_NAME);
    } catch (err) {
        // console.error(`Error connecting to mongoDB`, err);
        resolutions.reject(err);
        return;
    }
    // console.log("Connected successfully to mongoDB server");

    if (!isIterable(parsedResponse.hits)){
        resolutions.reject(new TypeError(`Data received from the API does not conform to expected structure`));
        client.close();
        return;
    }

    for (const hit of parsedResponse.hits) {
        let workingTitle = null;
        let workingURL = null;

        workingTitle = hit.title || hit.story_title;
        // Note: hit.url and hit.story_url can both be null, this is non-specified behaviour, set to '' and deal with it on frontend
        workingURL = hit.story_url || hit.url || '';
        if (workingTitle == null || workingTitle === '') {
            // console.warn(`Story with id: ${hit.story_id} has no title, skipping.`);
            continue;
        }
        // Note: hit.story_id can be null, this offers non-specified behaviour for unique story identification,
        // in it's stead we will use a hash of the title and the author
        // Please note that this API is quite... non-conformant, with many duplicate entries
        let newEntity = {
            internal_id: crypto.createHash('sha256').update(workingTitle).digest('hex'),
            story_id: hit.story_id,
            title: workingTitle,
            author: hit.author,
            created_at: hit.created_at,
            url: workingURL,
            dismissed: false,
        };
        // console.log(newEntity);
        let foundStory;
        try {
            foundStory = await collection.find({ internal_id: newEntity.internal_id }).limit(1).toArray();
        } catch (err) {
            // console.error(`Error querying database for story with internal_id: ${newEntity.internal_id}`, err);
            continue;
        }

        if (foundStory.length) {
            // console.warn(`Story with internal_id: ${newEntity.internal_id} is already registered`);
            continue;
        }

        let insertionResult;
        try {
            insertionResult = await collection.insertOne(newEntity);
        } catch (err) {
            // console.error(`Error inserting new story with id: ${newEntity.internal_id}`, err);
            continue;
        }
        if (insertionResult.insertedCount != 1) {
            console.error(`New story insertedCount does not match expected`);
        }
    }
    resolutions.resolve();
    client.close();
}
module.exports = {
    acquire,
    processData,
};

function isIterable(obj) {
    if (obj == null) {
        return false;
    }
    return typeof obj[Symbol.iterator] === 'function';
}