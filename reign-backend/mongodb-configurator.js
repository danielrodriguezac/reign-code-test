const DOCKER_MONGODB_URL = 'mongodb://mongo:27017';
const DOCKER_MONGODB_DATABASE_NAME = 'reign-code-test';
const DOCKER_MONGODB_STORIES_COLLECTION_NAME = 'stories';
const TESTING_MONGODB_URL = 'mongodb://localhost:27017';
const TESTING_MONGODB_DATABASE_NAME = 'reign-code-test-testing';
const TESTING_MONGODB_STORIES_COLLECTION_NAME = 'stories-testing';
const MONGODB_CLIENT_CONFIG = {
    useNewUrlParser: true,
    // FIXME: This property should be enabled, but a bug in the official driver forces
    // causes timeouts to stop working
    // useUnifiedTopology: true,
    connectTimeoutMS: 1000,
};
module.exports = (pEnv) => {
    let conf =  new mongoDBConfiguration(pEnv);
    return conf;
}
class mongoDBConfiguration {
    constructor(env = process.env) {
        this.MONGODB_CLIENT_CONFIG = MONGODB_CLIENT_CONFIG;
        if (env.NODE_ENV === 'production') {
            this.MONGODB_URL = DOCKER_MONGODB_URL;
            this.MONGODB_DATABASE_NAME = DOCKER_MONGODB_DATABASE_NAME;
            this.MONGODB_STORIES_COLLECTION_NAME = DOCKER_MONGODB_STORIES_COLLECTION_NAME;
            return;
        }
        this.MONGODB_URL = env.MONGODB_URL || TESTING_MONGODB_URL;
        this.MONGODB_DATABASE_NAME = env.MONGODB_DATABASE_NAME || TESTING_MONGODB_DATABASE_NAME;
        this.MONGODB_STORIES_COLLECTION_NAME = env.MONGODB_STORIES_COLLECTION_NAME || TESTING_MONGODB_STORIES_COLLECTION_NAME;
    }
}