// Native modules
const path = require('path');

// Vendor modules
const express = require('express');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const cors = require('cors');
// Compartmentalization
const storiesRouter = require('./routes/stories');
const dataAcquisition = require('./data-acquisition');

const API_REQUEST_INTERVAL = 60 * 60 * 1000;

let app = express();

app.use(logger('dev'));
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/stories', storiesRouter);

(async function (){
    while (1) {
        console.log(`Running...`);
        try {
            await dataAcquisition.acquire();
        } catch (err) {
            console.error(`Error acquiring API data: ${err}`, err);
            await delay(5000);
            continue;
        }
        await delay(API_REQUEST_INTERVAL);
    }
})();

module.exports = app;

function delay (ms = 5000) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve();
        }, ms);
    });
}