const assert = require('assert');
const EventEmitter = require('events');

const MongoClient = require('mongodb').MongoClient;
const { MONGODB_URL, MONGODB_DATABASE_NAME, MONGODB_STORIES_COLLECTION_NAME, MONGODB_CLIENT_CONFIG } = require('../mongodb-configurator.js')();

const storiesRouter = require('../routes/stories');

const dummyAPIData = require('../dummy/api-data.json');
const dummyDatabase = require('../dummy/database-dump.json');
// Dummy https equivalent interface
class httpsDummy extends EventEmitter {
    constructor(resDummy) {
        super();
        this.response = resDummy;
    }
    get(url, callback) {
        callback(this.response);
        return this;
    }
}
class responseDummy extends EventEmitter {
    constructor() {
        super();
    }
}
const dataAcq = require('../data-acquisition');
// await col.deleteMany({a:2});

describe('Data acquisition', function () {
    describe('#acquire', function () {
        it('Should correctly handle data from an https server', async function () {
            const res = new responseDummy()
            const https = new httpsDummy(res);
            setImmediate(() => {
                res.emit('data', JSON.stringify(dummyAPIData));
                res.emit('end');
            });
            let acqPromise;
            try {
                acqPromise = await dataAcq.acquire.apply({ https });
            } catch (err) {
                throw err;
            }
            return acqPromise;
        });
        it('Should throw on https protocol errors', async function () {
            const res = new responseDummy()
            const https = new httpsDummy(res);
            setImmediate(() => {
                https.emit('error', new Error(`Dummy error object for the https dummy interface`));

            });
            let acqPromise;
            try {
                acqPromise = await dataAcq.acquire.apply({ https });
            } catch (err) {
                assert.throws(() => {
                    throw err;
                });
            }
        });
    });
    describe('#processData', function () {
        beforeEach(async function () {
            // Connect to mongoDB
            const client = new MongoClient(MONGODB_URL, MONGODB_CLIENT_CONFIG);
            let database;
            let collection;
            try {
                await client.connect();
                database = client.db(MONGODB_DATABASE_NAME);
                collection = database.collection(MONGODB_STORIES_COLLECTION_NAME);
            } catch (err) {
                throw err;
            }

            try {
                await collection.deleteMany({});
            } catch (err) {
                throw err;
            }
            client.close();
        });
        it('Should correctly handle valid data and include it in the database', async function () {
            // Connect to mongoDB
            const client = new MongoClient(MONGODB_URL, MONGODB_CLIENT_CONFIG);
            let database;
            let collection;
            try {
                await client.connect();
                database = client.db(MONGODB_DATABASE_NAME);
                collection = database.collection(MONGODB_STORIES_COLLECTION_NAME);
            } catch (err) {
                throw err;
            }

            let test = new Promise((resolve, reject) => {
                dataAcq.processData(JSON.stringify(dummyAPIData), {resolve, reject});
            });
            let acqPromise;
            try {
                acqPromise = await test;
            } catch (err) {
                throw err;
            }
            
            let foundStories;
            try {
                foundStories = await collection.find({}, { sort: [['created_at', 'descending']] }).limit(1000).toArray();
            } catch (err) {
                throw err;
            }
            client.close();
            assert.equal(foundStories.length, 20);
        });
        it('Should throw on invalid JSON', async function () {
            let test = new Promise((resolve, reject) => {
                dataAcq.processData('this is invalid JSON', {resolve, reject});
            });
            let acqPromise;
            try {
                acqPromise = await test;
            } catch (err) {
                assert.throws(() => {
                    throw err;
                });
            }
        });
        it('Should throw on non-conformant data', async function () {
            let test = new Promise((resolve, reject) => {
                dataAcq.processData(JSON.stringify({thisIsSome: 'RandomObject'}), {resolve, reject});
            });
            let acqPromise;
            try {
                acqPromise = await test;
            } catch (err) {
                assert.throws(() => {
                    throw err;
                });
            }
        });
        it('Should dismiss articles without a valid title');
        it('Should dismiss articles whose internal_id is already on the database');
        it('Should continue on sudden database failure');
    });
});
describe('Data requests', function () {
    describe('#index', function () {
        it('Should send all articles ordered desc by date');
    });
    describe('#delete', function () {
        beforeEach(async function () {});
        it('Should change dismissed property of targeted story');
    });
});
describe('mongoDB configurator', function () {
    describe('#index', function () {
        it('Should give production values if NODE_ENV === "production"', function () {
            let configurationResult = require('../mongodb-configurator.js')(
                {
                    NODE_ENV: 'production'
                }
            );
            assert.equal(configurationResult.MONGODB_URL, 'mongodb://mongo:27017');
            assert.equal(configurationResult.MONGODB_DATABASE_NAME, 'reign-code-test');
            assert.equal(configurationResult.MONGODB_STORIES_COLLECTION_NAME, 'stories');
        });
        it('Should give testing values if NODE_ENV !== "production"', function () {
            let configurationResult = require('../mongodb-configurator.js')(
                {
                    NODE_ENV: 'protucdion'
                }
            );
            assert.equal(configurationResult.MONGODB_URL, 'mongodb://localhost:27017');
            assert.equal(configurationResult.MONGODB_DATABASE_NAME, 'reign-code-test-testing');
            assert.equal(configurationResult.MONGODB_STORIES_COLLECTION_NAME, 'stories-testing');
        });
        it('Should accept alternative database parameters if NODE_ENV !== "production"', function () {
            let configurationResult = require('../mongodb-configurator.js')(
                {
                    NODE_ENV: 'protucdion',
                    MONGODB_URL: 'a',
                    MONGODB_DATABASE_NAME: 'b',
                    MONGODB_STORIES_COLLECTION_NAME: 'c'
                }
            );
            assert.equal(configurationResult.MONGODB_URL, 'a');
            assert.equal(configurationResult.MONGODB_DATABASE_NAME, 'b');
            assert.equal(configurationResult.MONGODB_STORIES_COLLECTION_NAME, 'c');
        });
        it('Should give values if called with no parameters', function () {
            let configurationResult = require('../mongodb-configurator.js')();
            assert.equal(configurationResult.MONGODB_URL, process.env.MONGODB_URL || 'mongodb://localhost:27017');
            assert.equal(configurationResult.MONGODB_DATABASE_NAME, process.env.MONGODB_DATABASE_NAME || 'reign-code-test-testing');
            assert.equal(configurationResult.MONGODB_STORIES_COLLECTION_NAME, process.env.MONGODB_STORIES_COLLECTION_NAME || 'stories-testing');
        });
    });
});