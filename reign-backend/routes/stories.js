const assert = require('assert');
let express = require('express');
let router = express.Router();
const MongoClient = require('mongodb').MongoClient;
const { MONGODB_URL, MONGODB_DATABASE_NAME, MONGODB_STORIES_COLLECTION_NAME, MONGODB_CLIENT_CONFIG } = require('../mongodb-configurator.js')();

router.get('/', async function (req, res, next) {
    console.log(`Preparing stories for frontend`);
    // Connect to mongoDB
    const client = new MongoClient(MONGODB_URL, MONGODB_CLIENT_CONFIG);
    let database;
    let collection;
    try {
        await client.connect();
        database = client.db(MONGODB_DATABASE_NAME);
        collection = database.collection(MONGODB_STORIES_COLLECTION_NAME);
    } catch (err) {
        console.error(`Error connecting to mongoDB`, err);
        dataBuffer = '';
        requestingAPIData = false;
        res.status(500).end();
        return;
    }
    console.log("Connected successfully to mongoDB server");
    let foundStories;
    try {
        foundStories = await collection.find({}, { sort: [['created_at', 'descending']] }).limit(1000).toArray();
    } catch (err) {
        console.error(`Error querying database`, err);
        client.close();
        res.status(500).end();
        return;
    }
    res.end(JSON.stringify(foundStories));
    client.close();
});

router.get('/delete/:internal_id', async function (req, res, next) {
    console.log(`Deleting story with internal_id: ${req.params.internal_id}`);
    // Connect to mongoDB
    const client = new MongoClient(MONGODB_URL, MONGODB_CLIENT_CONFIG);
    let database;
    let collection;
    try {
        await client.connect();
        database = client.db(MONGODB_DATABASE_NAME);
        collection = database.collection(MONGODB_STORIES_COLLECTION_NAME);
    } catch (err) {
        console.error(`Error connecting to mongoDB`, err);
        dataBuffer = '';
        requestingAPIData = false;
        res.status(500).end();
        return;
    }

    console.log("Connected successfully to mongoDB server");
    let dismissedStory;
    try {
        dismissedStory = await collection.findOneAndUpdate(
            {
                internal_id: req.params.internal_id,
            }, {
            $set: {
                dismissed: true,
            }
        }, {
            returnOriginal: false,
            sort: [
                [
                    'created_at',
                    'descending',
                ]
            ],
            upsert: false,
        }
        );
        // console.log(dismissedStory);
        assert.equal(true, dismissedStory.lastErrorObject.updatedExisting);
        assert.equal(true, dismissedStory.value.dismissed);
    } catch (err) {
        console.error(`Error dismissing story with id: ${req.params.internal_id} ->`, err);
        client.close();
        res.status(404).end();
        return;
    }
    res.end(JSON.stringify(dismissedStory));
    client.close();
});

router.get('/restoreAll', async function (req, res, next) {
    console.log(`Restoring Stories...`);
    // Connect to mongoDB
    const client = new MongoClient(MONGODB_URL, MONGODB_CLIENT_CONFIG);
    let database;
    let collection;
    try {
        await client.connect();
        database = client.db(MONGODB_DATABASE_NAME);
        collection = database.collection(MONGODB_STORIES_COLLECTION_NAME);
    } catch (err) {
        console.error(`Error connecting to mongoDB`, err);
        dataBuffer = '';
        requestingAPIData = false;
        res.status(500).end();
        return;
    }

    console.log("Connected successfully to mongoDB server");
    let restoredStories;
    try {
        restoredStories = await collection.updateMany(
            {
                dismissed: true,
            }, {
            $set: {
                dismissed: false,
            }
        }, {
            returnOriginal: false,
            upsert: false,
        }
        );
        // console.log(restoredStories);
        // assert.equal(true, restoredStories.lastErrorObject.updatedExisting);
        // assert.equal(true, restoredStories.value.dismissed);
    } catch (err) {
        console.error(`Error restoring stories ->`, err);
        res.status(404).end();
        return;
    }
    res.end(JSON.stringify(restoredStories));
    client.close();
});

module.exports = router;
