import { Component } from '@angular/core';
import { Story } from './story'
import { StoryService } from "./story.service";
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'reign-frontend';
  loading: boolean = true;
  stories: Story[] = [];
  constructor(private storyService: StoryService) {
    // console.log(`app.component: constructor`);
  }
  getStories(): void {
    // console.log(`app.component: getStories`);
    this.storyService.getStories()
      .subscribe(stories => {
        // console.log(`app.component: getStories().subscribe()`);
        this.stories = stories.filter((story) => {
          // console.log(story);
          return !story.dismissed;
        })
      });
  }
  deleteStory(story: Story): void {
    // console.log(`app.component: deleteStory`);
    if (story.loading) {
      return;
    }
    story.loading = true;
    this.storyService.deleteStories(story)
      .subscribe(result => {
        // console.log(`app.component: deleteStory().subscribe()`);
        // console.log(result);
        if (result.length === 0) {
          return;
        }
        this.getStories();
        // this.stories = this.stories.filter((cStory) => {
        //   return cStory._id != story._id;
        // })
      });
  }
  onDelete(story: Story): void {
    // console.log(`Deleting request for id: ${story._id}`);
    this.deleteStory(story);
  }
  ngOnInit() {
    // console.log(`app.component: ngOnInit`);
    this.getStories();
    // setInterval(() => {
    //   this.getStories();
    // }, 5000);
  }
}