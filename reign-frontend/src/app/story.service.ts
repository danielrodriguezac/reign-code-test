import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { isDevMode } from '@angular/core';

import { Story } from "./story";

@Injectable({
  providedIn: 'root'
})
export class StoryService {

  constructor(
    private http: HttpClient,
  ) {
    if (isDevMode()) {
      this.storiesURL = 'http://localhost:3000/stories/';
    }
  }

  private storiesURL = 'stories/';

  getStories(): Observable<Story[]> {
    return this.http.get<Story[]>(this.storiesURL)
    .pipe(
      catchError(this.handleError<Story[]>('getStories', []))
    );
  }
  deleteStories(story: Story): Observable<Story[]> {
    return this.http.get<Story[]>(`${this.storiesURL}/delete/${story.internal_id}`)
    .pipe(
      catchError(this.handleError<Story[]>('deleteStories', []))
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    };
  }
}
