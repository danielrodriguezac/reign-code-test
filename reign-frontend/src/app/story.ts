export interface Story {
    _id: string,
    internal_id: string,
    story_id: string | number | null,
    title: string | null,
    author: string | null,
    created_at: string | null,
    url: string | null,
    dismissed: boolean,
    loading: boolean | null,
}

