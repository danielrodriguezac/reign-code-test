// Pipe implementation of desired created_at format, this has been inferred from the wireframe provided
import { Pipe, PipeTransform } from '@angular/core';

import { formatDistance, startOfYesterday, endOfYesterday, isSameDay, parseISO, format, addHours } from 'date-fns'
@Pipe({
  name: 'specDate'
})
export class SpecDatePipe implements PipeTransform {

  transform(value: string, ...args: unknown[]): string {
    let now = new Date();
    let target = parseISO(value);
    if (isSameDay(now, target)) {
      return format(target, 'h:mm aaaaa\'m\'');
    }
    if (isSameDay(target, startOfYesterday())) {
      return "Yesterday";
    }
    return format(target, "MMM d");;
  }

}
