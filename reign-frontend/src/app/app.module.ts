import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule }    from '@angular/common/http';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { StoriesComponent } from './stories/stories.component';
import { CustomDatePipe } from './custom.datepipe';

// Material
import { MatListModule } from '@angular/material/list';
import { MatSliderModule } from '@angular/material/slider';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatButtonModule } from '@angular/material/button'
import { MatIconModule } from '@angular/material/icon';
import { SpecDatePipe } from './spec-date.pipe';

@NgModule({
  declarations: [
    AppComponent,
    StoriesComponent,
    CustomDatePipe,
    SpecDatePipe,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatListModule,
    MatSliderModule,
    MatProgressSpinnerModule,
    MatButtonModule,
    MatIconModule,
    HttpClientModule,
  ],
  providers: [
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
